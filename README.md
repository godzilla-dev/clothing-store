# Installation & Configuration

### Installation and Configuration

**1. You can install Bagisto by using the GUI installer.**

##### a. Download zip from the link below:

[Download the latest release](https://github.com/bagisto/bagisto/releases/latest)

##### b. Extract the contents of zip and execute the project in your browser:

~~~
http(s)://localhost/bagisto/public
~~~

or

~~~
http(s)://example.com/public
~~~

**2. Or you can install Bagisto from your console.**

##### Execute these commands below, in order

~~~
1. composer create-project bagisto/bagisto-standard
~~~

~~~
2. php artisan bagisto:install
~~~

**3. Or you can install Bagisto from your console manually.**

1. configure your database:

	If the command above was completed successfully, then you’ll find the directory bagisto and all of its available code.

	Find file .env inside the bagisto directory and set the environment variables listed below:

	- APP_URL
	- DB_CONNECTION
	- DB_HOST
	- DB_PORT
	- DB_DATABASE
	- DB_USERNAME
	- DB_PASSWORD

	Although, mailer environment variables are also required to be set up as Bagisto requires emails to send to customers and admins for various built-in functionalities.

2. php artisan migrate
3. php artisan db:seed
4. php artisan vendor:publish
-> Press 0 and then press enter to publish all assets and
configurations.
5. php artisan storage:link
6. composer dump-autoload
7. php artisan key:generate

**To execute Bagisto**:

##### On server:

Warning: Before going into production mode we recommend you uninstall developer dependencies.
In order to do that, run the command below:

> composer install --no-dev
~~~
Open the specified entry point in your hosts file in your browser or make an entry in hosts file if not done.
~~~

##### On local:

~~~
php artisan serve
~~~


**How to log in as admin:**

> *http(s)://example.com/admin/login*
~~~
email:admin@example.com
password:admin123
~~~

**How to log in as customer:**

*You can directly register as customer and then login.*

> *http(s)://example.com/customer/register*